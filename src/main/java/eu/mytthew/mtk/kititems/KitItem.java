package eu.mytthew.mtk.kititems;

import lombok.Value;
import org.bukkit.Material;

@Value
public class KitItem {
	Material material;
	int amount;
}
