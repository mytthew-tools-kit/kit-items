package eu.mytthew.mtk.kititems;

import org.bukkit.Material;
import org.bukkit.command.CommandExecutor;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

public class Main extends JavaPlugin {
	private final Config config = new Config();
	private final Map<Player, LocalDateTime> times = new HashMap<>();

	public List<KitItem> getItemList() {
		return config.getItemList();
	}

	public int getDuration() {
		return config.getDuration();
	}

	public void setTime(Player player, LocalDateTime time) {
		times.put(player, time);
	}

	public LocalDateTime getTime(Player player) {
		return times.get(player);
	}

	public String getMessage(String title) {
		return config.getMessagesMap().get(title);
	}

	private KitItem createKitItem(String material, int amount) {
		return new KitItem(Material.matchMaterial(material), amount);
	}

	private void loadConfig() {
		Optional<ConfigurationSection> configurationSection = Optional.ofNullable(getConfig().getConfigurationSection("kit"));

		configurationSection.map(cs -> cs.getInt("duration"))
				.ifPresent(config::setDuration);

		configurationSection.map(cs -> cs.getList("items"))
				.stream()
				.flatMap(Collection::stream)
				.filter(item -> item instanceof Map)
				.map(item -> getConfig().createSection("x", (Map<?, ?>) item))
				.map(item -> createKitItem(item.getString("material"), item.getInt("amount")))
				.forEach(config.getItemList()::add);

		Optional.ofNullable(getConfig().getConfigurationSection("messages"))
				.ifPresent(messages -> messages.getKeys(false)
						.forEach(key -> config.getMessagesMap().put(key, messages.getString(key))));
	}

	@Override
	public void onEnable() {
		Map<String, CommandExecutor> commands = new HashMap<>();
		commands.put("kit", new KitCommand(this));
		commands.forEach((key, value) -> Objects.requireNonNull(getCommand(key)).setExecutor(value));
		saveDefaultConfig();
		loadConfig();
		System.out.println(config.getItemList());
	}
}
