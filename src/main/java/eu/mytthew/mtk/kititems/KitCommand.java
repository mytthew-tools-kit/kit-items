package eu.mytthew.mtk.kititems;

import lombok.Value;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Objects;

@Value
public class KitCommand implements CommandExecutor {
	Main main;

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (!(sender instanceof Player)) {
			return true;
		}

		Player player = (Player) sender;
		Inventory inventory = Bukkit.createInventory(null, 27, main.getMessage("kitInventoryTitle"));
		LocalDateTime timeCounter = main.getTime(player);
		if (timeCounter == null || timeCounter.isBefore(LocalDateTime.now())) {
			main.getItemList()
					.stream()
					.map(item -> new ItemStack(item.getMaterial(), item.getAmount() - countMissing(player, item.getMaterial())))
					.forEach(inventory::addItem);
			main.setTime(player, LocalDateTime.now().plusSeconds(main.getDuration()));
			player.openInventory(inventory);
		} else {
			Duration period = Duration.between(timeCounter, LocalDateTime.now());
			sender.sendMessage(main.getMessage("durationTimeMessage") + ": " + ChatColor.RED + -period.getSeconds());
		}
		return true;
	}

	private int countMissing(Player player, Material material) {
		return Arrays.stream(player.getInventory()
				.getContents())
				.filter(Objects::nonNull)
				.filter(itemStack -> itemStack.getType() == material)
				.mapToInt(ItemStack::getAmount)
				.sum();
	}
}
